//
//  ViewController.swift
//  appIOS
//
//  Created by Irving Delgado Silva on 01/03/21.
//

import UIKit
 
class ViewController: UIViewController {

    @IBOutlet weak var btn1: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func clickedbtn1(_ sender: Any) {
        
        btn1.text = "clicked"
    }
    
}

